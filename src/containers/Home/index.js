import React, {Component} from 'react'
import OwlCarousel, { Options } from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import'./style.css';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import { Link } from 'react-router-dom';
export class Home extends Component{
	
	
	render(){
		const psbslider = {
			responsive:{
                0: {
                    items: 2,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 6,
                },
			},
			margin:10,
			nav: true,
			dots: false,
			loop: true
		};
		const homeslider = {
			responsive:{
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 1,
                },
			},
			margin:10,
			nav: false,
			dots: true,
			loop: true
		};
		const catslider = {
			responsive:{
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 10,
                },
			},
			margin:10,
			nav: false,
			dots: true,
			loop: true
		};
	return(
		<div>
		<Header/>
		<OwlCarousel {...homeslider} className="owl-theme home-slider">
		<div className="item">
		<img src="https://themes.invints.com/prestashop/IT02/Gomart/modules/itimageslider/images/itslider-2.jpg"/>
		</div>
		</OwlCarousel>
		
		<div className="section">
		<div className="container-fluid">
		<div className="section-cat">
		<OwlCarousel {...catslider} className="owl-theme cat-slider">
		<div className="item">
		<div className="cat-box-list text-center">
		<div className="cat-box-list-icon">
		<img src="https://askbootstrap.com/preview/osahan-grocery-v1-4/img/small/1.jpg"/>
		</div>
		<div className="cbl-name">Fruits & Vegetable</div>
		<div className="cbl-count">65 Items</div>
		</div>
		</div>
		<div className="item">
		<div className="cat-box-list text-center">
		<div className="cat-box-list-icon">
		<img src="https://askbootstrap.com/preview/osahan-grocery-v1-4/img/small/2.jpg"/>
		</div>
		<div className="cbl-name">Grocery & Staples</div>
		<div className="cbl-count">65 Items</div>
		</div>
		</div>
		<div className="item">
		<div className="cat-box-list text-center">
		<div className="cat-box-list-icon">
		<img src="https://askbootstrap.com/preview/osahan-grocery-v1-4/img/small/3.jpg"/>
		</div>
		<div className="cbl-name">Beverages</div>
		<div className="cbl-count">6 Items</div>
		</div>
		</div>
		<div className="item">
		<div className="cat-box-list text-center">
		<div className="cat-box-list-icon">
		<img src="https://askbootstrap.com/preview/osahan-grocery-v1-4/img/small/8.jpg"/>
		</div>
		<div className="cbl-name">Breakfast & Dairy</div>
		<div className="cbl-count">6 Items</div>
		</div>
		</div>
		
		</OwlCarousel>
		</div>		
		<div className="add-banner mg-b-15">
			<div className="row row-md">
				<div className="col-lg-3">
				<img src="https://themes.invints.com/prestashop/IT02/Gomart/modules/itcustomhtml/views/img/mult-banner-4.jpg"/>
				</div>
				<div className="col-lg-3">
				<img src="https://themes.invints.com/prestashop/IT02/Gomart/modules/itcustomhtml/views/img/mult-banner-5.jpg"/>
				</div>
				<div className="col-lg-3">
				<img src="https://themes.invints.com/prestashop/IT02/Gomart/modules/itcustomhtml/views/img/mult-banner-6.jpg"/>
				</div>
				<div className="col-lg-3">
				<img src="https://themes.invints.com/prestashop/IT02/Gomart/modules/itcustomhtml/views/img/mult-banner-4.jpg"/>
				</div>
			</div>
		</div>
		<div className="psb-product-container">
				<div className="psbpc-title">
					<h2 className="tx-13 fb700 mg-b-0">Top Saver Today</h2>
					<Link to="/productlist" className="view-all">View All</Link>
				</div>
				<OwlCarousel {...psbslider} className="owl-theme psb-slider"
				>
				<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		
				</OwlCarousel>
			</div>
		<div className="add-banner mg-b-15 mg-t-10">
		<div className="row">
			<div className="col-lg-12">
				<img src="https://cdn.shopify.com/s/files/1/0033/7956/0537/files/Main-banner.jpg" className="img-res"/>
			</div>
		</div>
		</div>
		<div className="psb-product-container">
				<div className="psbpc-title">
					<h2 className="tx-13 fb700 mg-b-0">Top Saver Today</h2>
					<Link to="/productlist" className="view-all">View All</Link>
				</div>
				<OwlCarousel {...psbslider} className="owl-theme psb-slider"
				>
				<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		
				</OwlCarousel>
			</div>
		<div className="add-banner mg-b-15 mg-t-10">
		<div className="row row-lg">
		<div className="col-lg-4">
		<img src="https://templatetasarim.com/prestashop/Grocery/modules/inspsubbanners/images/sub-banner-1.jpg" className="img-res"/>
		</div>
		<div className="col-lg-4">
		<img src="https://templatetasarim.com/prestashop/Grocery/modules/inspsubbanners/images/sub-banner-2.jpg" className="img-res"/>
		<img src="https://templatetasarim.com/prestashop/Grocery/modules/inspsubbanners/images/sub-banner-3.jpg" className="img-res mg-t-15"/>
		</div>
		<div className="col-lg-4">
		<img src="https://templatetasarim.com/prestashop/Grocery/modules/inspsubbanners/images/sub-banner-4.jpg" className="img-res"/>
		</div>
		</div>
		</div>
		
		</div>
		</div>		
		<Footer/>
		</div>
	);
}
}
export default Home;
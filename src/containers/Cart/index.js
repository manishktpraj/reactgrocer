import React, {Component} from 'react'
import'./style.css';
import Header from '../../components/Header';

	export class Cart extends Component{
		render(){
	return(
		<div>
		<Header/>
		<div class="sub-header">
			<h1>My Cart <span class="tx-11">(1 items)</span></h1>
		</div>
		<div class="cart-section">
			<div class="container">
				<div class="row justify-content-md-center">
					<div class="col-lg-7 pd-0">
					<div class="alert alert-primary text-center" role="alert">Shop ₹500 more to get free delivery!</div>
					<div class="cart-details-box bg-white pd-15">
						<ul>
							<li>Sub Total<span>₹252</span></li>
							<li>Delivery Charges<span class="tx-color-theme">+₹22</span></li>
						</ul>
					</div>
					<div class="divider"></div>
					<div class="cart-item-box">
						<div class="cart-item-img">
							<img src='https://cdn.grofers.com/app/images/products/normal/pro_393.jpg'/>
						</div>
						<div class="cart-item-content">
							<div class="item-name">Surf Excel Matic Top Load Detergent Powder (Carton)</div>
							<div class="item-price">
								<span>₹282</span>
								<span class="old-price">₹282</span>
								<span class="off">20% off</span>
							</div>
							<div class="item-qty">
								<button><i class="icon ion-md-remove"></i></button>
								<span>2</span>
								<button><i class="icon ion-md-add"></i></button>
							</div>
						</div>
					</div>
					<div class="cart-item-box">
						<div class="cart-item-img">
							<img src='https://cdn.grofers.com/app/images/products/normal/pro_393.jpg'/>
						</div>
						<div class="cart-item-content">
							<div class="item-name">Surf Excel Matic Top Load Detergent Powder (Carton)</div>
							<div class="item-price">
								<span>₹282</span>
								<span class="old-price">₹282</span>
								<span class="off">20% off</span>
							</div>
							<div class="item-qty">
								<button><i class="icon ion-md-remove"></i></button>
								<span>2</span>
								<button><i class="icon ion-md-add"></i></button>
							</div>
						</div>
					</div>
					<div class="sprator-120"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="cart-footer">
		    <div class="container">
				<div class="row justify-content-md-center">
				<div class="col-lg-7 pd-0">
					<p class="text-center tx-color-03 ">Promo code can be applied on payment page</p>
				<button type="button" class="btn btn-primary btn-checkout btn-full">
					Proceed to Checkout
					<span class="cart-total pull-right">₹282 <i class="fas fa-angle-right mg-l-5"></i></span>
					</button>
				</div>
			</div>
			</div>
		</div>
		</div>
	);
}
}
export default Cart;
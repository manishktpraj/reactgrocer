import React, {Component} from 'react'
import OwlCarousel, { Options } from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import'./style.css';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import { Link } from 'react-router-dom';

export class ProductDetails extends Component{
    render(){
        const psbslider = {
			responsive:{
                0: {
                    items: 2,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 6,
                },
			},
			margin:10,
			nav: true,
			dots: false,
			loop: true
		};
        const productslider = {
			responsive:{
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 1,
                },
			},
			margin:10,
			nav: false,
			dots: true,
			loop: true
		};
	return(
        <div>
        <Header/>
        <div className="section">
        <div class="container-fluid">
            <div className="product-details-section">
            <div class="row">
                <div class="col-lg-5">
                <OwlCarousel {...productslider} className="owl-theme home-slider">
		        <div className="item">
		        <img src="https://cdn.grofers.com/app/images/products/full_screen/pro_389815.jpg"/>
		        </div>
                <div className="item">
		        <img src="https://cdn.grofers.com/app/images/products/sliding_image/389815a.jpg?ts=1582204020"/>
		        </div>
                <div className="item">
		        <img src="https://cdn.grofers.com/app/images/products/full_screen/pro_389815.jpg"/>
		        </div>
		        </OwlCarousel>
                </div>
                <div class="col-lg-7">
                    <div className="product-details-contant">
                        <h1>Family Far Arhar Dal/Toor Dal</h1>
                        <p>More By Everest</p>
                        <div class="product-item-price">
								<span>₹282</span>
								<span class="old-price">₹282</span>
								<span class="off">20% off</span>
						</div>
                        <p className="tx-color-orange">You Save: ₹28</p>
                        <h6 className="mg-b-10">Pack Sizes</h6>
                        <div className="pack-size">
                            <span className="active">100 g</span>
                            <span>500 g</span>
                        </div>
                        <div className="product-details-button">
                        <button type="button" class="btn btn-primary btn02">Add To Cart</button>
                        <div class="product-item-qty">
								<button><i class="icon ion-md-remove"></i></button>
								<span>2</span>
								<button><i class="icon ion-md-add"></i></button>
							</div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div className="product-des">
            <h2 class="tx-13 fb700 mg-b-0">Product Description</h2>
            <hr/>
            <p>We all love tomato ketchup and chutney which are not 
                only spicy or tangy in taste but also add an extra edge to 
                the flavours of your food. The quality of tomatoes determines the 
                taste and flavour of your favourite ketchup or any other dish or food in 
                which tomatoes are used. So, for the best quality tomatoes buy fresho tomato hybrid. 
                These are the best quality tomatoes available and are cultivated by the farmers by the 
                traditional method of agriculture without using any chemicals or harmful injections. 
                </p>
          </div>
            <div className="psb-product-container mg-t-15">
				<div className="psbpc-title">
					<h2 className="tx-13 fb700 mg-b-0">Top Saver Today</h2>
					<Link to="/productlist" className="view-all">View All</Link>
				</div>
				<OwlCarousel {...psbslider} className="owl-theme psb-slider">
				<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="add-cart">
					<button type="button" class="btn btn-primary btn-addcart">Add</button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		<div className="item">
				<div className="psb-product-list">
				<div class="psb-off">20% OFF</div>
				<div class="psb-product-img"><img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/></div>
				<div class="psb-product-content">
					<h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
					<p class="tx-color-03">500 g</p>
					<div class="psb-price-cart">
					<div class="psb-price">
					<span>₹282</span>
					<span class="old-price">₹302</span>
					</div>
					<div class="psb-list-qty">
						<button><i class="icon ion-md-remove"></i></button>
						<span>2</span>
						<button><i class="icon ion-md-add"></i></button>
					</div>
					</div>
					</div>
				</div>
				</div>
	    		
				</OwlCarousel>
			</div>
		
        </div>
        </div>
        <Footer/>
        </div>
    )
}
}

export default ProductDetails;
import React, {Component} from 'react'
import'./style.css';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import { Link } from 'react-router-dom';

export class ProductList extends Component{
    render(){
return(
    <div>
    <Header/>
    <div className="product-show-section">
    <div className="product-list-sidebar">
    <h6><strong>Filter by category</strong></h6>
    <hr/>
    <div className="slide-bar-list">
    <div className="sbl-title">Atta & Other Flours <i class="typcn typcn-plus"></i></div>
    <ul>
    <li><Link to="">Atta</Link></li>
    <li><Link to="">Besan & Sooji/Rava</Link></li>
    <li><Link to="">Other Flours</Link></li>
    </ul>
    <div className="sbl-title">Atta & Other Flours <i class="typcn typcn-plus"></i></div>
    <div className="sbl-title">Atta & Other Flours <i class="typcn typcn-plus"></i></div>
    </div>
    </div>
    <div className="product-list-section">
                <div className="product-list-heaader">
                <ul class="breadcrum">
                    <li><a href="#">Home</a></li>
                    <li class="active"><a href="javascript:;">Grocery & Staples</a></li>
                </ul>
                <h1 className="fb600 tx-12 mg-b-0 mg-t-5">Grocery & Staples</h1>
                </div>
                <div className="row row-md">
                    <div className="col-lg-3">
                        <Link to="/productdetails">
                        <div className="product-list-box">
                        <div className="plb-off">20% OFF</div>
                            <div className="plb-img">
                                <img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/>
                            </div>
                            <div className="plb-content">
                                <h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
                                <p className="tx-color-03">500 g</p>
                                <div className="price-cart">
                                    <div className="plb-price">
                                    <span>₹282</span>
								    <span class="old-price">₹302</span>
                                    </div>
                                    <div className="add-cart">
                                    <button type="button" class="btn btn-primary btn-addcart">Add To Cart</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </Link>
                    </div>
                     <div className="col-lg-3">
                        <Link to="/productdetails">
                        <div className="product-list-box">
                        <div className="plb-off">20% OFF</div>
                            <div className="plb-img">
                                <img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/>
                            </div>
                            <div className="plb-content">
                                <h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
                                <p className="tx-color-03">500 g</p>
                                <div className="price-cart">
                                    <div className="plb-price">
                                    <span>₹282</span>
								    <span class="old-price">₹302</span>
                                    </div>
                                    <div className="add-cart">
                                    <button type="button" class="btn btn-primary btn-addcart">Add To Cart</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </Link>
                    </div>
                    <div className="col-lg-3">
                        <div className="product-list-box">
                        <div className="plb-off">20% OFF</div>
                            <div className="plb-img">
                                <img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/>
                            </div>
                            <div className="plb-content">
                                <h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
                                <p className="tx-color-03">500 g</p>
                                <div className="price-cart">
                                    <div className="plb-price">
                                    <span>₹282</span>
								    <span class="old-price">₹302</span>
                                    </div>
                                    <div className="add-cart">
                                    <button type="button" class="btn btn-primary btn-addcart">Add To Cart</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="product-list-box">
                        <div className="plb-off">20% OFF</div>
                            <div className="plb-img">
                                <img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/>
                            </div>
                            <div className="plb-content">
                                <h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
                                <p className="tx-color-03">500 g</p>
                                <div className="price-cart">
                                    <div className="plb-price">
                                    <span>₹282</span>
								    <span class="old-price">₹302</span>
                                    </div>
                                    <div className="add-cart">
                                    <button type="button" class="btn btn-primary btn-addcart">Add To Cart</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="product-list-box">
                        <div className="plb-off">20% OFF</div>
                            <div className="plb-img">
                                <img src="https://cdn.grofers.com/app/images/products/normal/pro_389735.jpg?ts=1575012809"/>
                            </div>
                            <div className="plb-content">
                                <h4>Grofers Mother's Choice Steam Sonamasuri Rice</h4>
                                <p className="tx-color-03">500 g</p>
                                <div className="price-cart">
                                    <div className="plb-price">
                                    <span>₹282</span>
								    <span class="old-price">₹302</span>
                                    </div>
                                    <div className="add-cart">
                                    <button type="button" class="btn btn-primary btn-addcart">Add To Cart</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </div>
                    
                    </div>
                </div>
      </div>
    <Footer/>
    </div>
)
    }
}

export default ProductList;
import React from 'react';
import './App.css';
import Home from './containers/Home';
import Cart from './containers/Cart';
import ProductDetails from './containers/ProductDetails'
import ProductList from './containers/ProductList'
import {Route, Link} from "react-router-dom";

function App() {
  return (
    <div className="App">
     <Route exact path="/" component={Home}/>
     <Route exact path="/react" component={Home}/>
     <Route exact path="/Cart" component={Cart}/>
     <Route exact path="/ProductDetails" component={ProductDetails}/>
     <Route exact path="/ProductList" component={ProductList}/>
    </div>
  );
}

export default App;

import React, {Component} from 'react'
import'./style.css';
import { Link } from 'react-router-dom';

export class Footer extends Component{
    render(){
        return(
            <div>
                <div className="footer-top">
                <div className="container-fluid">
                    <div className="row row-md">
                        <div className="col-lg-3">
                        <div class="media footer-media">
                        <img class="mg-r-15" src={require('../../assets/img/icon/delivery.png')} alt="Generic placeholder image"/>
                        <div class="media-body">
                        <h5 class="mg-b-5 tx-10 tx-color-white">On time delivery</h5>
                        <p className="mg-b-0 tx-color-03 tx-07">ras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque.</p>
                        </div>
                        </div>
                        </div>
                        <div className="col-lg-3">
                        <div class="media footer-media">
                        <img class="mg-r-15" src={require('../../assets/img/icon/offer.png')} alt="Generic placeholder image"/>
                        <div class="media-body">
                        <h5 class="mg-b-5 tx-10 tx-color-white">Best Prices & Offers</h5>
                        <p className="mg-b-0 tx-color-03 tx-07">
                        Cheaper prices than your local supermarket, great cashback offers to top it off.
                            </p>
                        </div>
                        </div>
                        </div>
                        <div className="col-lg-3">
                        <div class="media footer-media">
                        <img class="mg-r-15" src={require('../../assets/img/icon/support.png')} alt="Generic placeholder image"/>
                        <div class="media-body">
                        <h5 class="mg-b-5 tx-10 tx-color-white">24/7 Support</h5>
                        <p className="mg-b-0 tx-color-03 tx-07">ras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque.</p>
                        </div>
                        </div>
                        </div>
                        <div className="col-lg-3">
                        <div class="media footer-media">
                        <img class="mg-r-15" src={require('../../assets/img/icon/return.png')} alt="Generic placeholder image"/>
                        <div class="media-body">
                        <h5 class="mg-b-5 tx-10 tx-color-white">Easy Returns</h5>
                        <p className="mg-b-0 tx-color-03 tx-07">ras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque.</p>
                        </div>
                        </div>
                        </div>
                   
                    </div>
                </div>
                </div>
                <footer>
                <div className="footer-middle">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-4">
                            <div className="footer-title">Popular Categories</div>
                            <div className="row">
                                <div className="col-lg-6">
                                        <div className="footer-list">
                                            <ul>
                                                <li><Link to="">Grocery & Staples</Link></li>
                                                <li><Link to="">Rice</Link></li>
                                                <li><Link to="">Fruits & Vegetables</Link></li>
                                                <li><Link to="">Cooking Oil</Link></li>
                                                <li><Link to="">Detergent Powders</Link></li>
                                                <li><Link to="">Noodles & Pasta</Link></li>
                                                <li><Link to="">Soft Drinks</Link></li>
                                                <li><Link to="">Baby Diapers</Link></li>
                                            </ul>
                                        </div>
                                </div>
                                <div className="col-lg-6">
                                        <div className="footer-list">
                                            <ul>
                                                <li><Link to="">Salt & Sugar</Link></li>
                                                <li><Link to="">Tea</Link></li>
                                                <li><Link to="">Dry Fruits</Link></li>
                                                <li><Link to="">Biscuits & Cookies</Link></li>
                                                <li><Link to="">Atta</Link></li>
                                                <li><Link to="">Toor Dal</Link></li>
                                                <li><Link to="">Ghee</Link></li>
                                                <li><Link to="">Soaps & Bars</Link></li>
                                            </ul>
                                        </div>
                                </div>
                            </div>
                            </div>
                            <div className="col-lg-2">
                                <div className="footer-title">Useful Links</div>
                                <div className="footer-list">
                                            <ul>
                                                <li><Link to="">About Us</Link></li>
                                                <li><Link to="">Help & Support</Link></li>
                                                <li><Link to="">Faq's</Link></li>
                                                <li><Link to="">Privacy Policy</Link></li>
                                                <li><Link to="">Terms & Conditions</Link></li>
                                                <li><Link to="">Cancellation & Returns</Link></li>
                                                <li><Link to="">Latest News & Updates</Link></li>
                                                <li><Link to="">Contact Us</Link></li>
                                            </ul>
                                        </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="container-fluid">
                    <div className="footer-bottom">
                        <div className="row">
                        <div className="col-lg-3">
                            <h6 className="mg-b-15 tx-color-white">Download App</h6>
                            <img src={require('../../assets/img/Google-App-store-icon.png')} className="wd95"/>
                            <img src={require('../../assets/img/Apple-App-store-icon.png')} className="wd95 mg-l-10"/>
                            </div>
                            
                            <div className="col-lg-3">
                            <h6 className="mg-b-15 tx-color-white">Follow Us On</h6>
                            <div className="social-list">
                                <ul>
                                    <li><a href="#"><i class="typcn typcn-social-facebook-circular"></i></a></li>
                                    <li><a href="#"><i class="typcn typcn-social-twitter-circular"></i></a></li>
                                    <li><a href="#"><i class="typcn typcn-social-instagram-circular"></i></a></li>
                                    <li><a href="#"><i class="typcn typcn-social-linkedin-circular"></i></a></li>
                                </ul>
                            </div>
                            </div>
                            <div className="col-lg-3">
                            <h6 className="mg-b-15 tx-color-white">Follow Us On</h6>
                            <div className="social-list">
                                <ul>
                                    <li><a href="#"><i class="typcn typcn-social-facebook-circular"></i></a></li>
                                    <li><a href="#"><i class="typcn typcn-social-twitter-circular"></i></a></li>
                                    <li><a href="#"><i class="typcn typcn-social-instagram-circular"></i></a></li>
                                    <li><a href="#"><i class="typcn typcn-social-linkedin-circular"></i></a></li>
                                </ul>
                            </div>
                            </div>
                            <div className="col-lg-3">
                                <h6 className="mg-b-15 tx-color-white">Payment Option</h6>
                                <img src={require('../../assets/img/payment.png')} className="img-res"/>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div className="copyright text-center tx-color-03 tx-08">
                    <div className="container">
                       
                        © 2020 - Grocery Pvt. Ltd.
                        </div>
                    </div>
                  
                </footer>
            </div>
        )
    }

}

export default Footer
import React, {Component} from 'react'
import'./style.css';
import {Button, Modal} from 'react-bootstrap'

export class AppModal extends Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return(
          <Modal
          {...this.props}
          size="sm"
        >
          
          <Modal.Body class="pb-0">
            <div class="login-box">
              <button onClick={this.props.onHide} type="button" class="btn-close">×</button>
              <div class="login-box-header bg-theme">
                <h5 class="tx-color-white">Login & Sign Up</h5>
                <p class="mg-b-0 tx-color-white">Welcome back! Please login & sign up to continue.</p>
              </div>
              <div class="login-form">
              <div class="login-form-box mobile-number mg-b-15">
                <div class="country-code">+91-</div>
              <input type="text"></input>
              </div>
              <div class="login-form-box">
              <button type="button" class="btn btn-primary btn01 btn-full">Next</button>
              </div>
              </div>
            </div>
            
          </Modal.Body>
          
        </Modal>
        );
    }
}
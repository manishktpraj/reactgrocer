import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {Button, ButtonToolbar} from 'react-bootstrap'
import {AppModal} from '../AppModal/appmodal';
/**
* @author
* @function Header
**/

export class Header extends Component{
    constructor(props){
        super(props);
        this.state ={deps:[], addModalShow : false}
    }
    render(){
        let addModalClose =() => this.setState({addModalShow:false});
  return(
      <div>
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header-left">
                    <a href="#" class="logo"><img src={require('../../assets/img/logo-white.png')} alt="" class="wd115"/></a>
                    <a href="#" class="location">
                        <i class="typcn typcn-location-outline mg-r-5"></i>Jaipur<i class="fas fa-angle-down mg-l-10 mg-t-1"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 pless-left">
                   <div class="header-search">
                       <input type="text" placeholder="Search for products, brands & more..."/>
                       <div class="icon-search"><i class="icon-search4"></i></div>
                   </div>
                </div>
                <div class="col-lg-3">
                    <div class="header-right">
                        <ul>
                        <li>
                            <Link to="#" onClick={()=> this.setState({addModalShow:true})}>
                            
                            <img src={require('../../assets/img/icon/user.png')} alt="" class="wd32 mg-r-10"/>
                            Login / Sign Up
                            </Link>
                            </li>
                            <li class="cart">
                                <Link to="/cart">
                                <img src={require('../../assets/img/icon/cart.png')} alt="" class="wd32 mg-r-10"/>
                                My Cart
                                <div class="cart-count">2</div>
                                </Link>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <AppModal
    show={this.state.addModalShow}
    onHide={addModalClose}
    />
    </div>
   )

 }
}
export default Header